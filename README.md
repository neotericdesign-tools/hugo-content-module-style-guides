# Hugo Content Module - Style Guides

A standardized, markdown typographic style guide for use in Hugo projects.

## Installation

First, setup your Hugo parent project as a Hugo module with `hugo mod init <projectname>`. Then, add this module to your `config.toml`, and mount its resources to the directory of your choice.

```toml
[module]
    [[module.imports]]
        path = "gitlab.com/neotericdesign-tools/hugo-content-module-style-guides.git"
        [[module.imports.mounts]]
            source = "content/style-guides"
            target = "content/style-guides"
```
